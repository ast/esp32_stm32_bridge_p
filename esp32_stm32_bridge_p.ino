#include <WiFi.h>

#define buttonPin 0 
#define LEDPin 5

// Network information
char* ssid = "XXXXXXXXXXXX";
const char* password = "XXXXXXXXXXXXX";

// ThingSpeak Settings
char server[] = "api.thingspeak.com";
String writeAPIKey = "XXXXXXXXXXXXXXXXX";

// Constants
const unsigned long postingInterval = 15L * 1000L;

// Global Variables
unsigned long lastConnectionTime = 0;
int measurementNumber = 0;

// A 1 for each value in the telemetry String which should be send to the webserver
int values_to_plot[] = {0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0};

void setup(){
  
    Serial.begin(115200);
    Serial.println("SETUP");
    pinMode(buttonPin,INPUT);
    pinMode(LEDPin, OUTPUT);
    Serial.println("connectWiFi");
    connectWiFi();
    Serial.println("connectWiFi DONE");
    
}

void loop(){
  const int numberPoints = 7;
  float wifiStrength;

  // In each loop, make sure there is always an internet connection.
    if (WiFi.status() != WL_CONNECTED) { 
        connectWiFi();
    }
    String storedData = "";
    if( Serial.available()){ // if new data is coming from the HW Serial
      Serial.println("Serial.available");
    int str_len = 0;
       while(Serial.available())          // reading data into char array
       {
         delay(2);  // Delay to allow byte to arrive in input buffer
         char inChar = Serial.read();
         storedData += inChar;
         //Serial1.println("Stored Char: ");
         str_len++;
       }
    Serial.print("Stored Data: ");
    Serial.println(storedData);
    String prefix = storedData.substring(0, 5);
    Serial.print("Prefix: ");
    Serial.println(prefix);
    if (prefix == "WiFi,"){
      Serial.print("Got Data To Send");
      unsigned char datToSend[str_len];
      for (int i=0; i<str_len; i++){
        datToSend[i+1] = (unsigned char) storedData[i];
      }
      // field1
      String str_for_thingspeak = get_str_for_thingspeak(storedData);
      Serial.println("Will send: " + str_for_thingspeak);
      httpRequest(str_for_thingspeak);
      delay(100);
    }

    }
}

void connectWiFi(){
    WiFi.begin(ssid, password);
    delay(400);
    while (WiFi.status() != WL_CONNECTED){
        delay(3000);
        Serial.print(".");
        Serial.print(WiFi.status());
    }

    // Show the user a connection is successful.
    Serial.println("Connected");
    blinkX(5,50);  
}

void httpRequest(String dataToSend) {

    WiFiClient client;
    
    if (!client.connect(server, 80)){
      
        Serial.println("connection failed");
        lastConnectionTime = millis();
        client.stop();
        return;     
    }
    
    else{
        
        // create data string to send to ThingSpeak
        String data = dataToSend;
        //String data = "field1=" + String(field1Data) + "&field2=" + String(field2Data); //shows how to include additional field data in http post
        
        // POST data to ThingSpeak
        if (client.connect(server, 80)) {
          
            client.println("POST /update HTTP/1.1");
            client.println("Host: api.thingspeak.com");
            client.println("Connection: close");
            client.println("User-Agent: ESP32WiFi/1.1");
            client.println("X-THINGSPEAKAPIKEY: "+writeAPIKey);
            client.println("Content-Type: application/x-www-form-urlencoded");
            client.print("Content-Length: ");
            client.print(data.length());
            client.print("\n\n");
            client.print(data);
            
            //Serial.println("RSSI = " + String(field1Data));
            lastConnectionTime = millis();   
        }
    }
    client.stop();
}

// Take a number of measurements of the WiFi strength and return the average result.
int getStrength(int points){
    long rssi = 0;
    long averageRSSI=0;
    
    for (int i=0;i < points;i++){
        rssi += WiFi.RSSI();
        delay(20);
    }

   averageRSSI=rssi/points;
    return averageRSSI;
}

// Flash the LED a variable number of times with a variable delay.
void blinkX(int numTimes, int delayTime){ 
    for (int g=0;g < numTimes;g++){

        // Turn the LED on and wait.
        digitalWrite(LEDPin, HIGH);  
        delay(delayTime);

        // Turn the LED off and wait.
        digitalWrite(LEDPin, LOW);
        delay(delayTime);
        
    }
}


// Gets a telemetry string from the stm32 and creates a string for the thingspeak-api
String get_str_for_thingspeak(String storedData){
  String str_for_thingspeak = "";
  int val_cnt = 1; // Counts the fields for the server
  int prev_i = 0;
  int val_ind = 0;
  storedData = storedData + ",";
  for (int i = 0; i < storedData.length(); i++) {
  if (storedData.substring(i, i+1) == ",") {
    String temp_value = storedData.substring(prev_i+1, i);
    if ((val_ind>1) and (values_to_plot[val_ind]==1)){
      str_for_thingspeak = str_for_thingspeak + "field" + (val_cnt) + "=" + temp_value + "&";
      val_cnt++;
    }
    prev_i = i;    
    val_ind++;
  }
}
  return str_for_thingspeak.substring(0, str_for_thingspeak.length()-1); 
}







