/*
  WriteMultipleFields
  
  Description: Writes values to fields 1,2,3,4 and status in a single ThingSpeak update every 20 seconds.
  
  Hardware: ESP32 based boards
  
  !!! IMPORTANT - Modify the secrets.h file for this project with your network connection and ThingSpeak channel details. !!!
  
  Note:
  - Requires installation of EPS32 core. See https://github.com/espressif/arduino-esp32/blob/master/docs/arduino-ide/boards_manager.md for details. 
  - Select the target hardware from the Tools->Board menu
  - This example is written for a network using WPA encryption. For WEP or WPA, change the WiFi.begin() call accordingly.
  
  ThingSpeak ( https://www.thingspeak.com ) is an analytic IoT platform service that allows you to aggregate, visualize, and 
  analyze live data streams in the cloud. Visit https://www.thingspeak.com to sign up for a free account and create a channel.  
  
  Documentation for the ThingSpeak Communication Library for Arduino is in the README.md folder where the library was installed.
  See https://www.mathworks.com/help/thingspeak/index.html for the full ThingSpeak documentation.
  
  For licensing information, see the accompanying license file.
  
  Copyright 2018, The MathWorks, Inc.
*/

#include "ThingSpeak.h"
#include "secrets.h"
#include <WiFi.h>

char ssid[] = SECRET_SSID;   // your network SSID (name) 
char pass[] = SECRET_PASS;   // your network password
int keyIndex = 0;            // your network key Index number (needed only for WEP)
WiFiClient  client;

unsigned long myChannelNumber = SECRET_CH_ID;
const char * myWriteAPIKey = SECRET_WRITE_APIKEY;

// A 1 for each value in the telemetry String which should be send to the webserver
int values_to_plot[] = {0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 0};
#define VAL_CNT 8
float values_to_send[VAL_CNT];


// Initialize our values
float number1 = 0.0;
float number2 = 0.0;
float number3 = 0.0;
float number4 = 0.0;
String myStatus = "";

void setup() {
  Serial.begin(9600);  //Initialize serial // Was 115200

  WiFi.mode(WIFI_STA);   
  ThingSpeak.begin(client);  // Initialize ThingSpeak


}

void loop() {

  // Connect or reconnect to WiFi
  if(WiFi.status() != WL_CONNECTED){
    Serial.print("Attempting to connect to SSID: ");
    Serial.println(SECRET_SSID);
    while(WiFi.status() != WL_CONNECTED){
      WiFi.begin(ssid, pass);  // Connect to WPA/WPA2 network. Change this line if using open or WEP network
      Serial.print(".");
      delay(5000);     
    } 
    Serial.println("\nConnected.");
  }

    String storedData = "";
    if( Serial.available()){ // if new data is coming from the HW Serial
      Serial.println("Serial.available");
    int str_len = 0;
       while(Serial.available())          // reading data into char array
       {
         delay(26);  // Delay to allow byte to arrive in input buffer
         char inChar = Serial.read();
         storedData += inChar;
         //Serial1.println("Stored Char: ");
         str_len++;
       }
    Serial.print("Stored Data: ");
    Serial.println(storedData);
    String prefix = storedData.substring(0, 5);
    Serial.print("Prefix: ");
    Serial.println(prefix);
    if (prefix == "WiFi,"){
      Serial.print("Got Data To Send");
      unsigned char datToSend[str_len];
      for (int i=0; i<str_len; i++){
        datToSend[i+1] = (unsigned char) storedData[i];
      }
      // field1
      String str_for_thingspeak = get_str_for_thingspeak(storedData, values_to_send);
      Serial.println("Will send: " + str_for_thingspeak);
      //httpRequest(str_for_thingspeak);

      // set the fields with the values
      /*ThingSpeak.setField(1, values_to_send[0]);
      ThingSpeak.setField(2, values_to_send[1]);
      ThingSpeak.setField(3, values_to_send[2]);
      ThingSpeak.setField(4, values_to_send[3]);*/

      int val_cnt = VAL_CNT;
      for (int i=0; i<val_cnt; i++){
        ThingSpeak.setField(i, values_to_send[i]);
      }
      
      Serial.println("Fields SET");
      
      // write to the ThingSpeak channel
      int x = ThingSpeak.writeFields(myChannelNumber, myWriteAPIKey);
      Serial.println("ThingSpeak.writeFields Done");
      if(x == 200){
        Serial.println("Channel update successful.");
      }
      else{
        Serial.println("Problem updating channel. HTTP error code " + String(x));
      }
      
      delay(100);
    }

    }



  

}

// Gets a telemetry string from the stm32 and creates a string for the thingspeak-api
String get_str_for_thingspeak(String storedData, float *values_to_send){
  String str_for_thingspeak = "";
  int val_cnt = 1; // Counts the fields for the server
  int prev_i = 0;
  int val_ind = 0;
  storedData = storedData + ",";
  for (int i = 0; i < storedData.length(); i++) {
  if (storedData.substring(i, i+1) == ",") {
    String temp_value = storedData.substring(prev_i+1, i);
    if ((val_ind>1) and (values_to_plot[val_ind]==1)){
      str_for_thingspeak = str_for_thingspeak + "field" + (val_cnt) + "=" + temp_value + "&";
      values_to_send[val_cnt] = temp_value.toFloat();
      val_cnt++;
    }
    prev_i = i;    
    val_ind++;
  }
}
  return str_for_thingspeak.substring(0, str_for_thingspeak.length()-1); 
}


